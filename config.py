from datetime import datetime
from configparser import ConfigParser

"""Config variables"""

HOST = "127.0.0.1"
PORT = 60000
CODE = "utf-8"
HEADER_SIZE = 1024
VERSION = "Version 1.1.0"
START_TIME = datetime.now()
CONNECTION = True


def config(filename, section):
    parser = ConfigParser()
    parser.read(filename)
    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))
    return db
