import json
import socket
from config import *


class Client:
    def __init__(self, host, port, connection):
        self.host = host
        self.port = port
        self.connection = connection
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def serialize_msg(self, msg):
        msg_json = json.dumps(msg)
        msg_encode = msg_json.encode(CODE)
        self.sock.sendall(msg_encode)

    def start(self):
        self.sock.connect((self.host, self.port))
        print("\n.:: Welcome to the server! Type 'help' for info. ::.\n")

        while self.connection:
            user_input = input("===>>> ").lower()
            comm_dic = {"command": user_input}

            match user_input:
                case "login":
                    username = input("Type your username: ").lower()
                    password = input("Type your password: ")
                    comm_dic.update(username=username, password=password)
                    self.serialize_msg(comm_dic)
                case "logged_out":
                    self.serialize_msg(comm_dic)
                case "add_user":
                    username = input("Type your username: ").lower()
                    password = input("Type your password (minimum of six characters in length): ")
                    while len(password) < 6:
                        print("Password is too short. Try again.")
                        password = input("Type your password (minimum of six characters in length): ")
                    comm_dic.update(username=username, password=password)
                    self.serialize_msg(comm_dic)
                case "remove_user":
                    username = input("Enter username to be remove from db: ").lower()
                    comm_dic.update(username=username)
                    self.serialize_msg(comm_dic)
                case "send_msg":
                    username = input("Enter the user to whom you want to send the message: ").lower()
                    message = input("Enter the message, no longer than 255 characters: ")
                    if len(message) > 255:
                        print(f"Message is too long. Currently length of message is {len(message)}")
                        message = input("Enter the message, no longer than 255 characters: ")
                        print(type(message))
                    comm_dic.update(username=username, message=message)
                    self.serialize_msg(comm_dic)
                case "read_msg":
                    username = input("Enter the user whose messages you want to read: ").lower()
                    comm_dic.update(username=username)
                    self.serialize_msg(comm_dic)
                case "delete_msg":
                    username = input("Specify the user whose messages you wish to delete: ").lower()
                    comm_dic.update(username=username)
                    self.serialize_msg(comm_dic)
                case "help":
                    self.serialize_msg(comm_dic)
                case "uptime":
                    self.serialize_msg(comm_dic)
                case "info":
                    self.serialize_msg(comm_dic)
                case _:
                    self.serialize_msg(comm_dic)
            data = self.sock.recv(HEADER_SIZE).decode(CODE)
            print(f"[RECEIVED FROM SERVER] {data}")


if __name__ == "__main__":
    client = Client(HOST, PORT, CONNECTION)
    client.start()
