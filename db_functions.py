import psycopg2
from psycopg2 import Error
from config import config


class SqlConnection:
    def __init__(self):
        self.connection = SqlConnection.connect_to_db(self)

    def connect_to_db(self, filename='database.ini', section='postgresql'):
        try:
            params = config(filename, section)
            self.connection = psycopg2.connect(**params)
            return self.connection
        except (Exception, Error) as error:
            print("Error while connecting to the database:", error)

    def close_connection_with_db(self):
        self.connection.close()

    def insert_data_into_table(self, name, password, is_admin=False):
        if SqlConnection.user_exists(self, name):
            return "User with this name already exists in the database."
        try:
            sql = """INSERT INTO users(name, password, is_admin)
                     VALUES(%s, %s, %s)"""
            data = (name, password, is_admin)
            cursor = self.connection.cursor()
            cursor.execute(sql, data)
            self.connection.commit()
            return f"Record inserted successfully user has been created."
        except (Exception, psycopg2.Error) as error:
            self.connection.rollback()
            print(f"[ERROR WHILE INSERT DATA TO POSTGRESQL] ===>>>", {error})
            return "Unable to insert data to PostgreSQL."

    def user_exists(self, name):
        try:
            if self.connection:
                sql = "SELECT COUNT(*) FROM users WHERE name = %s"
                data = (name,)
                cursor = self.connection.cursor()
                cursor.execute(sql, data)
                count = cursor.fetchone()[0]
                return count > 0
        except (Exception, psycopg2.Error) as error:
            print("[ERROR WHILE CHECKING USER EXISTENCE] ===>>>", error)
            return False

    def checking_data_in_db(self, name, password):
        try:
            sql = "SELECT EXISTS (SELECT 1 FROM users WHERE name = %s AND password = %s);"
            cursor = self.connection.cursor()
            cursor.execute(sql, (name, password))
            result = cursor.fetchone()
            return result[0]
        except (Exception, psycopg2.Error) as error:
            self.connection.rollback()
            print(f"[ERROR WHILE CHECKING DATA TO POSTGRESQL] ===>>>", {error})
            return None

    def delete_user(self, name):
        try:
            sql = "DELETE FROM users WHERE name = %s;"
            cursor = self.connection.cursor()
            cursor.execute(sql, (name,))
            self.connection.commit()
            return "User deleted from database."
        except (Exception, psycopg2.Error) as error:
            self.connection.rollback()
            print(f"[ERROR WHILE DELETING DATA FROM POSTGRESQL] ===>>>", {error})

    def send_msg_to_user(self, name, message):
        try:
            cursor = self.connection.cursor()
            sql = "SELECT name, inbox FROM users WHERE name = %s;"
            cursor.execute(sql, (name,))
            user_data = cursor.fetchone()
            if user_data:
                user_name, inbox = user_data[0], user_data[1]
                if inbox is not None and len(inbox) >= 5:
                    return f"User {user_name} inbox is full. You cannot send any more messages."
                sql = "UPDATE users SET inbox = array_cat(inbox, %s) WHERE name = %s;"
                cursor.execute(sql, ([message], name))
                self.connection.commit()
                return f"Message to user {user_name} has been sent."
            else:
                return f"User {name} do not exist."
        except (Exception, psycopg2.Error) as error:
            self.connection.rollback()
            print(f"[ERROR WHILE SENDING MESSAGE] ===>>> {error}")
            return f"Unable to send a message."

    def delete_oldest_msg(self, name):
        try:
            cursor = self.connection.cursor()
            sql = "UPDATE users SET inbox = inbox[2:] WHERE name = %s;"
            cursor.execute(sql, (name, ))
            self.connection.commit()
            return f"The oldest message for user {name} has been deleted."
        except (Exception, psycopg2.Error) as error:
            self.connection.rollback()
            print(f"[ERROR WHILE DELETING A MESSAGE] ===>>> {error}")
            return f"Unable to delete the oldest message."

    def read_user_msg(self, name):
        try:
            cursor = self.connection.cursor()
            sql = "SELECT inbox FROM users WHERE name = %s;"

            cursor.execute(sql, (name,))
            result = cursor.fetchone()
            messages = result[0]
            if messages:
                return f"Messages for user {name}: {messages}"
            else:
                return f"User {name} inbox is empty."
        except (Exception, psycopg2.Error) as error:
            self.connection.rollback()
            print(f"[ERROR WHILE READING MESSAGES] ===>>> {error}")
            return f"Unable to read messages"
