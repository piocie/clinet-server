from db_functions import SqlConnection


class User:
    def __init__(self):
        self.db = SqlConnection()
        self.command = None
        self.username = None
        self.password = None
        self.message = None
        self.user_logged = False
        self.admin_logged = False
        self.logged_on_user = 0
        self.db_connection = False

    def process_command(self, command):
        self.command = command
        self.username = command.get("username")
        self.password = command.get("password")
        self.message = command.get("message")

    def check_compliance(self):
        if not self.db_connection:
            self.db.connect_to_db()
            self.db_connection = True
        return self.db.checking_data_in_db(self.username, self.password)

    def login(self):
        if self.user_logged or self.admin_logged:
            return f"User <<{self.logged_on_user}>> is already logged in"
        else:
            if self.check_compliance():
                if self.username == "admin":
                    self.admin_logged = True
                    self.logged_on_user = self.username
                    return f"User <<{self.username}>> logged, welcome to the server"
                else:
                    self.user_logged = True
                    self.logged_on_user = self.username
                    return f"User <<{self.username}>> logged, welcome to the server"
            else:
                return "Unable to log in, please check username and password and try again."

    def logged_out(self):
        self.user_logged = False
        self.admin_logged = False
        self.logged_on_user = 0
        self.db.close_connection_with_db()
        self.db_connection = False
        return "You have successfully logged out, connection with DB terminated"

    def add_user(self):
        if not self.db_connection or not self.admin_logged:
            return "Please log in as admin to add a new user."
        return self.db.insert_data_into_table(self.username, self.password)

    def delete_user(self):
        if self.admin_logged:
            return self.db.delete_user(self.username)
        else:
            return "Only the admin has access to delete users"

    def send_msg(self):
        return self.db.send_msg_to_user(self.username, self.message)

    def read_msg(self):
        if self.username == self.logged_on_user or self.logged_on_user == "admin":
            return self.db.read_user_msg(self.username)
        else:
            return "Please login to read your messages"

    def delete_msg(self):
        if not (self.admin_logged or self.user_logged):
            return "Please login to be able delete message"

        if self.username == self.logged_on_user or self.logged_on_user == "admin":
            return self.db.delete_oldest_msg(self.username)
        return "You can only delete your messages"
