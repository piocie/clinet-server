import socket
import json
from config import *
from user_management import User
from db_functions import SqlConnection


class Server:
    def __init__(self, host, port, connection, db=SqlConnection()):
        self.host = host
        self.port = port
        self.connection = connection
        self.db = db
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.commands = {
            "login": "Login user to the server",
            "logout": "Logged out from server",
            "add_user": "Add a new user",
            "remove_user": "Remove user from db (only admin)",
            "send_msg": "Send a meesage to other user ",
            "read_msg": "Read user messages",
            "delete_msg": "Delete message from selected user",
            "uptime": "Show server life time",
            "info": "Return server version and date when it was created",
            "stop": "Interrupts server and client work",
            "help": "Show available commands with descriptions"
        }

    def execute_command(self, value, user):
        match value.get("command"):
            case "help":
                return self.srv_help()
            case "uptime":
                return self.srv_uptime()
            case "info":
                return self.srv_info()
            case "stop":
                return self.srv_stop()
            case "add_user":
                return user.add_user()
            case "remove_user":
                return user.delete_user()
            case "send_msg":
                return user.send_msg()
            case "delete_msg":
                return user.delete_msg()
            case "read_msg":
                return user.read_msg()
            case "login":
                return user.login()
            case "logout":
                return user.logged_out()
            case _:
                return self.srv_unknown_command()

    @staticmethod
    def receive_json(msg):
        msg_dic = json.loads(msg)
        return msg_dic

    @staticmethod
    def send_json(msg):
        msg_dic = json.dumps(msg, indent=3)
        return msg_dic

    def srv_unknown_command(self):
        msg_dic = {"Unknown commend": "type 'help' to get a list of available commands"}
        return self.send_json(msg_dic)

    def srv_stop(self):
        self.db.close_connection_with_db()
        stop_dic = {"Server and DB": "was stopped by client || CONNECTION TERMINATED"}

        return self.send_json(stop_dic)

    def srv_info(self):
        info_dic = {
            "Info": f"Server created date : {str(START_TIME).split('.')[0]}",
            "Server version": VERSION
        }
        return self.send_json(info_dic)

    def srv_help(self):
        return self.send_json(self.commands)

    def srv_uptime(self):
        now = datetime.now()
        live_time = now - START_TIME
        uptime_dic = {"Server is live for": str(live_time).split(".")[0]}
        return self.send_json(uptime_dic)

    def srv_start(self):
        self.sock.bind((self.host, self.port))
        self.sock.listen()
        communication_socket, address = self.sock.accept()

        with communication_socket:
            print(f"[CONNECTION RECEIVED] {address[0]} | {address[1]}")

            user = User()

            while self.connection:
                date = communication_socket.recv(HEADER_SIZE).decode(CODE)
                command = self.receive_json(date)
                print(f"[RECEIVED FROM CLIENT] {command}")
                user.process_command(command)
                if command.get("command") == "stop".lower():
                    communication_socket.send(self.execute_command(command, user).encode(CODE))
                    break
                communication_socket.send(self.execute_command(command, user).encode(CODE))


if __name__ == "__main__":
    server = Server(HOST, PORT, CONNECTION)
    server.srv_start()
