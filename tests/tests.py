import unittest
from unittest.mock import Mock
import json
from client import Client
from config import HOST, PORT, CODE
from server import Server
from user_management import User
from db_functions import SqlConnection


class TestClient(unittest.TestCase):
    def setUp(self):
        self.client = Client(HOST, PORT, Mock())
        self.fake_socket = Mock()
        self.client.sock = self.fake_socket

    def test_serialize_msg(self):
        message = {'key': 'value'}
        self.client.serialize_msg(message)
        sent_data = self.fake_socket.sendall.call_args[0][0]
        expected_data = json.dumps(message).encode(CODE)
        self.assertEqual(sent_data, expected_data)


class TestServer(unittest.TestCase):

    def setUp(self):
        self.server = Server("test_host", 12345, True)
        self.user = User()
        self.db = SqlConnection()

    def test_srv_unknown_command(self):
        result = self.server.srv_unknown_command()
        self.assertIn("Unknown commend", result)

    def test_srv_stop(self):
        result = self.server.srv_stop()
        self.assertIn("Server and DB", result)

    def test_srv_info(self):
        result = self.server.srv_info()
        self.assertIn("Info", result)
        self.assertIn("Server version", result)

    def test_srv_help(self):
        result = self.server.srv_help()
        self.assertIn("help", result)
        self.assertIn("uptime", result)

    def test_srv_uptime(self):
        result = self.server.srv_uptime()
        self.assertIn("Server is live for", result)

    def test_execute_command_help(self):
        command = {"command": "help"}
        result = self.server.execute_command(command, self.user)
        self.assertIn("help", result)

    def test_execute_command_stop(self):
        command = {"command": "stop"}
        result = self.server.execute_command(command, self.user)
        self.assertIn("Server and DB", result)

    def test_receive_json(self):
        json_data = '{"command": "help"}'
        result = self.server.receive_json(json_data)
        self.assertEqual(result['command'], "help")

    def test_send_json(self):
        data = {"message": "Test message"}
        result = self.server.send_json(data)
        self.assertIn("message", result)
        self.assertIn("Test message", result)


class TestDatabase(unittest.TestCase):
    def setUp(self):
        self.db = SqlConnection()
        self.filename = "test_database.ini"
        self.section = "test_postgresql"
        self.name = "test_user"
        self.password = "test_password"

    def test_connect_to_db(self):
        connection = self.db.connect_to_db(self.filename, self.section)
        self.assertIsNotNone(connection)

    def test_insert_data_into_table(self):
        data = self.db.insert_data_into_table(self.name, self.password, self.filename)
        result = "Record inserted successfully user has been created."
        self.assertEqual(data, result)

    def test_delete_user(self):
        data = self.db.delete_user(self.name, self.filename, self.section)
        result = "User deleted from database."
        self.assertEqual(data, result)

    def test_send_msg_to_user(self):
        data = self.db.send_msg_to_user(self.name, "test_msg", self.filename)
        result = f"Message to user {self.name} has been sent."
        self.assertEqual(data, result)

    def test_send_msg_to_user_fail(self):
        data = self.db.send_msg_to_user("fail_user", "test_msg", self.filename)
        result = "User fail_user do not exist."
        self.assertEqual(data, result)

    def test_delete_oldest_msg(self):
        data = self.db.delete_oldest_msg(self.name, self.filename)
        result = f"The oldest message for user {self.name} has been deleted."
        self.assertEqual(data, result)

    def test_read_user_msg(self):
        data = self.db.read_user_msg(self.name, self.filename, self.section)
        result = f"User {self.name} inbox is empty."
        self.assertEqual(data, result)


if __name__ == '__main__':
    unittest.main()
